using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

public struct DetectionData : IDetectionData
{
   public float3 origin;
   public float3 direction;
   public float range;
   public float angleRange;
   public int effectedLayers;
   public int DetectionType;
   public string DetectionInterface ;
   public float Radius { get => range;  }
   public float3 Origin => origin;
   public int EffectedLayers => effectedLayers;
   public float Range => range;
   public float3 Direction => direction;
   public string InterfaceName => DetectionInterface;
}
