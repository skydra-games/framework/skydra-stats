using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDetectionDataAOECircular : IDetectionAOE
{
    public float Radius {get;}
    public float AngleRange{get;}
   
}
