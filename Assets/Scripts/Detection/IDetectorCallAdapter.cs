using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDetectorCallAdapter 
{
   public void callDetector(IDetectionData data);
}
