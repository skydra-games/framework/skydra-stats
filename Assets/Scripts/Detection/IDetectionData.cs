using Unity.Mathematics;
public interface IDetectionData 
{
   public float3 Origin{get;}
   public float3 Direction{get;}
   public string InterfaceName{get;}
   public int EffectedLayers{get;}
   public float Range{get;}
}
