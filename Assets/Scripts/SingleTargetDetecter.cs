using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Direction Single Target Detector", menuName = "Scriptables/Detectors/Single Target Detector")]
public class SingleTargetDetecter : GenericTargetDetector<RaycastHit, SingleTargetDirectionalDetectorInfo>
{
    public override RaycastHit Detect(SingleTargetDirectionalDetectorInfo info)
    {
        lastDetectionInfo = info;

        if (!Physics.Raycast(info.Origin, info.Direction, out result, info.Range, info.LayerMask))
        {
            result = default;
        }
        
        return result;
    }
    
}

[System.Serializable]
public struct SingleTargetDirectionalDetectorInfo 
{
    public float Range;
    public Vector3 Origin;
    public Vector3 Direction;
    public LayerMask LayerMask;
    public string OwnerID;
}

