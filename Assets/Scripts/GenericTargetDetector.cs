using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GenericTargetDetector<DetectionResult, DetectorInfo> : ScriptableObject
{
    [SerializeField] protected DetectionResult result;
    [SerializeField] protected DetectorInfo lastDetectionInfo;
    public abstract DetectionResult Detect(DetectorInfo info);
    
}
