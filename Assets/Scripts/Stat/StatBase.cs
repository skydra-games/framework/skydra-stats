using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Character Attribute", menuName = "Scriptables/Stat")]
public class StatBase : ScriptableObject
{
    /// <summary>
    /// Old value, New Value
    /// </summary>
    public Action<float, float> OnValueChanged;
    [SerializeField]private float _value;
    [SerializeField] private float _oldValue;
    [SerializeField] private StatId _statId ;

    public int statId => _statId.Id;

    public float Value
    {
        get { return _value; }
        set
        {
            if (_value == value)
                return;

            _oldValue = _value;
            _value = value;

            OnValueChanged?.Invoke(_oldValue, _value);
        }
    }
}
