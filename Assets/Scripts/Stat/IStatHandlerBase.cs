using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStatContainer 
{
    public StatBase DefaultValue { get; set; }
    public StatBase CurrentValue { get; set; }
    public void ResetToDefault();
    
}

