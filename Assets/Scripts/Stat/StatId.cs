using UnityEngine;

[CreateAssetMenu(fileName = "New StatID ", menuName = "Scriptables/StatId")]
public class StatId : ScriptableObject
{
   public int Id ;

}
