using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Attack : MonoBehaviour
{
    [SerializeField] 
    IDetectorCallAdapter detectionAdapter;
    void Start()
    {
        detectionAdapter = GetComponent<IDetectorCallAdapter>();

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        detectionAdapter.callDetector(createDetectionData());

    }

    public DetectionData createDetectionData()
    {

        return  new DetectionData{
            angleRange = 1, 
            DetectionInterface = "TestDetection",
            direction = new float3(0f,1f,1f)
        };

    }
}
