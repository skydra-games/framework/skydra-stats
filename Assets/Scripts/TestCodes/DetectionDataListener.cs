using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionDataListener : MonoBehaviour
{
    public DetectorAdapter detectorAdapter;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        
        if(detectorAdapter)
        detectorAdapter.DetectorSent+= DetectionArrived;
    }


    public void DetectionArrived(EventArgs args)
    {

        DetectionEventArgs dea = args as DetectionEventArgs ; 
        Debug.Log(name + " - Received -" +  dea.data.InterfaceName);
    }
    
}
