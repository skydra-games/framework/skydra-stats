using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectorAdapter : MonoBehaviour, IDetectorCallAdapter
{

    public Action<EventArgs> DetectorSent; 
    
    public void callDetector(IDetectionData data)
    {
      Debug.Log("DetectorArrived : " + data.InterfaceName);
      DetectorSent?.Invoke(new DetectionEventArgs(data));
    }

    // Start is called before the first frame update
    
}
public class  DetectionEventArgs: EventArgs
{

public IDetectionData data;
    public DetectionEventArgs(IDetectionData data)
    {

        this.data = data;

    }
}