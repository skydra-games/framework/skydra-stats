using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterDataManager : MonoBehaviour
{
    [SerializeField] CharacterAttributes attributes;
    [SerializeField] Dictionary<int,StatHandler> statHandlers = new Dictionary<int,StatHandler>();

    private void Awake()
    {
        CreateHandlers();
    }

    private void CreateHandlers()
    {
        for (int i = 0; i < attributes.Stats.Count; i++)
        {
            StatHandler handler = new(attributes.Stats[i]);

            statHandlers.Add(handler.DefaultValue.statId, handler);// DUSELT
        }
    }
}
[Serializable]
public class StatHandler : IStatContainer
{
    [SerializeField] private StatBase defaultValue;
    [SerializeField] private StatBase currentValue;
    public StatBase DefaultValue { get => defaultValue; set => defaultValue = value; }
    public StatBase CurrentValue { get => currentValue; set => currentValue = value; }
    public StatHandler(StatBase maxValue)
    {
        defaultValue = maxValue;
        currentValue = ScriptableObject.Instantiate(maxValue);

        ResetToDefault();
    }
    public void ResetToDefault()
    {
        CurrentValue.Value = DefaultValue.Value;
    }
}
