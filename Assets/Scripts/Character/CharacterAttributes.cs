using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Character Attribute", menuName ="Scriptables/Character Atrribute")]
public class CharacterAttributes : ScriptableObject
{
    public List<StatBase> Stats = new List<StatBase>();
    

}
