using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Circular Target Detector", menuName = "Scriptables/Detectors/Multiple Circular Target Detector")]
public class MultipleTargetInCircleDetector : GenericTargetDetector<RaycastHit[], MultipleCircularTargetDetectorInfo>
{
    public override RaycastHit[] Detect(MultipleCircularTargetDetectorInfo info)
    {
        lastDetectionInfo = info;
        result = Physics.SphereCastAll(info.Origin, info.Radius, info.Direction, info.Range, info.LayerMask);

        return result;
    }
    
}

[System.Serializable]
public struct MultipleCircularTargetDetectorInfo 
{
    public float Range;
    public Vector3 Origin;
    public Vector3 Direction;
    public float Radius;
    public LayerMask LayerMask;
    public string OwnerID;
}