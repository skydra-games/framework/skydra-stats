using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Not Used
public class DetectorBase : ScriptableObject
{
    [SerializeField] protected string ownerID;
    
    public void SetOwnerID(string id)
    {
        ownerID = id;
    }
}
// Not Used
public interface IDetector
{

}
// Not Used
public interface IDetectSingleDirectional : IDetector
{
    object GetDetected(SingleTargetDirectionalDetectorInfo detectorInfo);
}
// Not Used
public interface IDetectMultiple : IDetector
{
    object[] GetDetecteds(MultipleCircularTargetDetectorInfo info);
}